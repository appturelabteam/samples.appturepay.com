<h1>Auth Endpoint Samples</h1>

<h2>Access Token</h2>
<p>Sample for authenticating with the client_credentials grant on the <code>auth/access_token</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "appture_pay_web";
$clientSecret = "";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// calling getSession will automatically authenticate with the client_credentials grant
if( $api->getSession() !== null ) {
    var_dump($api->getSession());
}
?>
</pre>

<h2>Revoke Token</h2>
<p>Sample for revoking an access token using the <code>auth/revoke_token</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "appture_pay_web";
$clientSecret = "";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// calling getSession will automatically authenticate with the client_credentials grant
if( $api->getSession() !== null ) {
    // revoke the token by calling authRevokeToken
    var_dump($api->authRevokeToken());
}
?>
</pre>
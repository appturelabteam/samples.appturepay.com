<?php
/**
 * 
 *
 * @author Dewald
 */
namespace ApptureLab;
class WebRequest {
    
    public $apiUrl = null;
    protected $ch;
    protected $lastResponse;
    protected $lastCurlInfo;
    
    private static $totalTime = 0;
    private static $calls = array();
    
    protected function getCurlInfo() {
        if($this->lastCurlInfo) {
            return $this->lastCurlInfo;
        } else {
            return null;
        }
    }
    
    public static function getCalls(){
        return WebRequest::$calls;
    }
    
    public static function getTotalTime(){
        return WebRequest::$totalTime;
    }
    
    public static function getLocalIndex() {
        return str_ireplace("index.php", "", filter_input(INPUT_SERVER, 'PHP_SELF'));
    }
    
    public static function getIndex() {
        return (filter_input(INPUT_SERVER, 'HTTPS') ? 'https' : 'http')."://". filter_input(INPUT_SERVER, 'HTTP_HOST'). WebRequest::getLocalIndex();
    }

    protected function doWebRequest($method, $url, $payload, $headers = array(), $verbose = false) {
        
        $time = microtime(true);
        
        // our curl handle (initialize if required)
        if (is_null($this->ch)) {
            $this->ch = curl_init();
        }
        
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_URL, $this->apiUrl.$url);
        
        if(strtoupper($method) === "GET") {
            curl_setopt($this->ch, CURLOPT_HTTPGET, true);
        } else if(strtoupper($method) === "POST") {
            curl_setopt($this->ch, CURLOPT_POST, true);
        } else if(strtoupper($method) === "PUT") {
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "PUT");
        } else if(strtoupper($method) === "DELETE") {
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        } else if(strtoupper($method) === "OPTIONS") {
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "OPTIONS");
        }
        
        if($payload && count($payload)) {
            if($headers !== null) {
                if(array_search("Content-Type: application/json",$headers) !== false) {
                    $jsonPayload = json_encode($payload);
                    if($jsonPayload === false) {
                        return array("error" => "JSON Encode failed", $payload);
                    } else {
                        $payload = $jsonPayload;
                    }
                } else if(array_search("Content-Type: multipart/form-data",$headers) !== false) {
                    $payload = $payload;
                } else {
                    $payload = http_build_query($payload);
                }
            } else {
                $payload = http_build_query($payload);
            }
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
        }
        
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, stripos($this->apiUrl,"https") === false || stripos(WebRequest::getIndex(),"https") === false ? false : true);
        
        if(strtoupper($method) === "PUT"
                && array_search("Content-Type: application/json",$headers) !== false) {
            $headers[] = 'Content-Length: '. strlen($payload);
        }
        
        if($headers && count($headers)) {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        }
        
        if($verbose) {
            return curl_exec($this->ch);
        } else {
            curl_setopt($this->ch, CURLINFO_HEADER_OUT, 1);
            curl_setopt($this->ch, CURLOPT_HEADER, 1);
        }
        
        $this->lastResponse = curl_exec($this->ch);
        $this->lastCurlInfo = curl_getinfo($this->ch);
        
        // Retudn headers seperatly from the Response Body
        $header_size = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);
        $headers = substr($this->lastResponse, 0, $header_size);
        $body = substr($this->lastResponse, $header_size);
        
        curl_close($this->ch);
        $this->ch = null;
        
        WebRequest::$totalTime += (microtime(true)-$time);
        WebRequest::$calls[] = array("url" => $this->apiUrl.$url, "method" => $method, "time" => microtime(true)-$time);
        
        if ($this->lastResponse === false)  {
            return array("error" => "CURL Failed - Check URL", "curl" => $this->lastCurlInfo);
        }
        
        $dec = json_decode($body, true);
        if (!$dec) {
            return array("error" => "Invalid JSON returned", "curl" => $this->lastCurlInfo, "response" => $this->lastResponse);
        }

        return $dec;
    }
}
<h1>Transaction Endpoint Samples</h1>

<h2>List Transactions</h2>
<p>Sample for listing all transactions using the <code>transaction</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// authenticate as a user with the password grant
$api->authPassword("{your account username}", "{your account password}");

// variable for keeping the response
$transactionGetResponse = null;

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the transaction endpoint using the GET method
    $transactionGetResponse = $api->transactionGet();
    var_dump($transactionGetResponse);
}
?>
</pre>

<h2>Create Transaction</h2>
<p>Sample for creating a transaction using the <code>transaction</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$transactionPostResponse = null;

// values to pass as request body
$data = array(
    "client_id" => $clientId,
    "transaction_type" => "DB",
    "recurring_term" => "1",
    "reference" => "TRANS01",
    "description" => "Test transaction",
    "total" => "999.99"
);

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the transaction/{id} endpoint using the GET method
    $transactionPostResponse = $api->transactionPost($data);
    var_dump($transactionPostResponse);
}
?>
</pre>

<h2>Get Transaction</h2>
<p>Sample for loading a specific transaction using the <code>transaction/{id}</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$transactionGetSpecificResponse = null;

// transaction id or identifier
$id = ($transactionPostResponse !== null && $transactionPostResponse["success"] ? $transactionPostResponse["data"]["id"] : "{transaction id or identifier}");

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the transaction/{id} endpoint using the GET method
    $transactionGetSpecificResponse = $api->transactionGetSpecific($id);
    var_dump($transactionGetSpecificResponse);
}
?>
</pre>

<h2>Delete Transaction</h2>
<p>Sample for deleting a transaction using the <code>transaction/{id}</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$transactionDeleteResponse = null;

// transaction id or identifier
$id = ($transactionPostResponse !== null && $transactionPostResponse["success"] ? $transactionPostResponse["data"]["id"] : "{transaction id or identifier}");

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the transaction/{id} endpoint using the GET method
    $transactionDeleteResponse = $api->transactionDelete($id);
    var_dump($transactionDeleteResponse);
}
?>
</pre>

<h2>Capture Transaction</h2>
<p>Sample for capturing an amount on a Preauthorised transaction using the <code>transaction/{id}/capture</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// First, create a Preauthorised transaction

// variable for keeping the response
$transactionPostResponse = null;

// values to pass as request body
$data = array(
    "client_id" => $clientId,
    "transaction_type" => "PA",
    "recurring_term" => "1",
    "reference" => "TRANSPA01",
    "description" => "Test PA transaction",
    "total" => "2199.99"
);

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the transaction/{id} endpoint using the GET method
    $transactionPostResponse = $api->transactionPost($data);
}

// make sure it was a success
if($transactionPostResponse !== null && $transactionPostResponse["success"]) {
    
    // ... Now, at this point the user will need to be redirected to the gateway in
    // ... order to complete the transaction before it can be captured.
    
    // We can capture part of the preauthorized amount and also do this multiple
    // times. See the amount below is less than the transaction total.
    
    // variable for keeping the response
    $transactionPutCaptureResponse = null;

    // transaction id or identifier
    $id = $transactionPostResponse["data"]["id"];
    
    // values to pass as request body
    $data = array(
        "amount" => "199.99",
    );

    // make sure we are successfully authenticated
    if( $api->getSession() !== null ) {
        // then make the call to the transaction/{id}/capture endpoint using the PUT method
        $transactionPutCaptureResponse = $api->transactionPutCapture($id,$data);
        var_dump($transactionPutCaptureResponse);
    }
}
?>
</pre>

<h2>Reverse Transaction</h2>
<p>Sample for reversing an amount on a Preauthorised transaction using the <code>transaction/{id}/reverse</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// First, create a Preauthorised transaction

// variable for keeping the response
$transactionPostResponse = null;

// values to pass as request body
$data = array(
    "client_id" => $clientId,
    "transaction_type" => "PA",
    "recurring_term" => "1",
    "reference" => "TRANSPA01",
    "description" => "Test PA transaction",
    "total" => "2199.99"
);

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the transaction/{id} endpoint using the GET method
    $transactionPostResponse = $api->transactionPost($data);
}

// make sure it was a success
if($transactionPostResponse !== null && $transactionPostResponse["success"]) {
    
    // ... Now, at this point the user will need to be redirected to the gateway in
    // ... order to complete the transaction before it can be reversed.
    
    // We can reverse part of the preauthorized amount and also do this multiple
    // times. See the amount below is less than the transaction total.
    
    // variable for keeping the response
    $transactionPutReverseResponse = null;

    // transaction id or identifier
    $id = $transactionPostResponse["data"]["id"];
    
    // values to pass as request body
    $data = array(
        "amount" => "199.99",
    );

    // make sure we are successfully authenticated
    if( $api->getSession() !== null ) {
        // then make the call to the transaction/{id}/reverse endpoint using the PUT method
        $transactionPutReverseResponse = $api->transactionPutReverse($id,$data);
        var_dump($transactionPutReverseResponse);
    }
}
?>
</pre>
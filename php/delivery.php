<h1>Delivery Endpoint Samples</h1>

<h2>List Deliveries</h2>
<p>Sample for listing all deliveries using the <code>delivery</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// authenticate as a user with the password grant
$api->authPassword("{your account username}", "{your account password}");

// variable for keeping the response
$deliveryGetResponse = null;

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery endpoint using the GET method
    $deliveryGetResponse = $api->deliveryGet();
    var_dump($deliveryGetResponse);
}
?>
</pre>

<h2>Create Delivery</h2>
<p>Sample for creating a delivery using the <code>delivery</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$deliveryPostResponse = null;

// values to pass as request body
$data = array(
    "delivery_courier_checked" => 0,
    "delivery_contact_name" => "Test",
    "delivery_contact_number_1" => "1231231234",
    "delivery_street" => "2 Street Ave",
    "delivery_suburb" => "Suburb",
    "delivery_city" => "City",
    "delivery_province" => "Gauteng",
    "delivery_country" => "South Africa",
    "delivery_postal_code" => "1501"
);

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id} endpoint using the GET method
    $deliveryPostResponse = $api->deliveryPost($data);
    var_dump($deliveryPostResponse);
}
?>
</pre>

<?php
if($deliveryPostResponse === null || ($deliveryPostResponse !== null && !$deliveryPostResponse["success"])) {
    exit("Fatal Error - cannot continue");
}
?>

<h2>Get Delivery</h2>
<p>Sample for loading a specific delivery using the <code>delivery/{id}</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$deliveryGetSpecificResponse = null;

// delivery id
$id = ($deliveryPostResponse !== null && $deliveryPostResponse["success"] ? $deliveryPostResponse["data"]["id"] : "{delivery id}");

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id} endpoint using the GET method
    $deliveryGetSpecificResponse = $api->deliveryGetSpecific($id);
    var_dump($deliveryGetSpecificResponse);
}
?>
</pre>

<h2>Update Delivery</h2>
<p>Sample for updating a delivery using the <code>delivery/{id}</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$deliveryPutResponse = null;

// delivery id
$id = ($deliveryPostResponse !== null && $deliveryPostResponse["success"] ? $deliveryPostResponse["data"]["id"] : "{delivery id}");

// values to pass as request body
$data = array(
    "delivery_contact_name" => "Test Update",
    "delivery_contact_number_1" => "1231230000",
    "delivery_street" => "12 Street Ave",
    "delivery_suburb" => "Suburb",
    "delivery_city" => "City",
    "delivery_province" => "Gauteng",
    "delivery_country" => "South Africa",
    "delivery_postal_code" => "1501"
);

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id} endpoint using the PUT method
    $deliveryPutResponse = $api->deliveryPut($id, $data);
    var_dump($deliveryPutResponse);
}
?>
</pre>

<h2>Delete Delivery</h2>
<p>Sample for deleting a delivery using the <code>delivery/{id}</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$deliveryDeleteResponse = null;

// delivery id
$id = ($deliveryPostResponse !== null && $deliveryPostResponse["success"] ? $deliveryPostResponse["data"]["id"] : "{delivery id}");

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id} endpoint using the GET method
    $deliveryDeleteResponse = $api->deliveryDelete($id);
    var_dump($deliveryDeleteResponse);
}
?>
</pre>

<h2>Check Postal Code</h2>
<p>Sample for checking a postal code using the <code>delivery/check_postal_code/{postal_code}</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$deliveryGetCheckPostalCodeResponse = null;

// postal code to check
$postalCode = "1501";

// values to pass as paramters
$data = array(
    "city" => "Benoni",
);

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id} endpoint using the GET method
    $deliveryGetCheckPostalCodeResponse = $api->deliveryGetCheckPostalCode($postalCode,$data);
    var_dump($deliveryGetCheckPostalCodeResponse);
}
?>
</pre>

<h2>Create Delivery Quote</h2>
<p>Sample for quoting a delivery using the <code>delivery/{id}/quote</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// First, create a delivery.

// variable for keeping the response
$deliveryPostResponse = null;

// values to pass as request body
$data = array(
    "delivery_courier_checked" => 0,
    "delivery_contact_name" => "Test",
    "delivery_contact_number_1" => "1231231234",
    "delivery_street" => "2 Street Ave",
    "delivery_suburb" => "Suburb",
    "delivery_city" => "City",
    "delivery_province" => "Gauteng",
    "delivery_country" => "South Africa",
    "delivery_postal_code" => "1501"
);

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id} endpoint using the GET method
    $deliveryPostResponse = $api->deliveryPost($data);
    
    // make sure the post succeeded
    if($deliveryPostResponse !== null && $deliveryPostResponse["success"]) {
        
        // Now we can call the quote endpoint for this delivery.
        
        // variable for keeping the response
        $deliveryPostQuoteResponse = null;
        
        // transaction id or identifier
        $id = $deliveryPostResponse["data"]["id"];

        // values to pass as request body
        $data = array(
            "value" => "199",
            "products" => array(
                array(
                    "quantity" => 1, // integer		  Y	number of pieces
                    "description" => "String with max length of 30", // string[30]	  N	freight description
                    "height" => 100, // integer		  Y	dimension 1 in millimeters
                    "width" => 250, // integer		  Y	dimension 2 in millimeters
                    "length" => 100, // integer		  Y	dimension 3 in millimeters
                    "weight" => 1.0 // float		  Y	mass in kilograms
                )
            ),
            "insurance" => 1
        );

        // then make the call to the delivery/{id}/quote endpoint using the POST method
        $deliveryPostQuoteResponse = $api->deliveryPostQuote($id, $data);
        var_dump($deliveryPostQuoteResponse);
        
    }
}
?>
</pre>

<h2>Update Delivery Quote Service</h2>
<p>Sample for updating the selected service for a delivery quote using the <code>delivery/{id}/quote</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// First, create a delivery.

// variable for keeping the response
$deliveryPostResponse = null;

// values to pass as request body
$data = array(
    "delivery_courier_checked" => 0,
    "delivery_contact_name" => "Test",
    "delivery_contact_number_1" => "1231231234",
    "delivery_street" => "2 Street Ave",
    "delivery_suburb" => "Suburb",
    "delivery_city" => "City",
    "delivery_province" => "Gauteng",
    "delivery_country" => "South Africa",
    "delivery_postal_code" => "1501"
);

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id} endpoint using the GET method
    $deliveryPostResponse = $api->deliveryPost($data);
    
    // make sure the post succeeded
    if($deliveryPostResponse !== null && $deliveryPostResponse["success"]) {
        
        // Next we can call the quote endpoint for this delivery.
        
        // variable for keeping the response
        $deliveryPostQuoteResponse = null;
        
        // delivery id
        $id = $deliveryPostResponse["data"]["id"];

        // values to pass as request body
        $data = array(
            "value" => "199",
            "products" => array(
                array(
                    "quantity" => 1, // integer		  Y	number of pieces
                    "description" => "String with max length of 30", // string[30]	  N	freight description
                    "height" => 100, // integer		  Y	dimension 1 in millimeters
                    "width" => 250, // integer		  Y	dimension 2 in millimeters
                    "length" => 100, // integer		  Y	dimension 3 in millimeters
                    "weight" => 1.0 // float		  Y	mass in kilograms
                )
            ),
            "insurance" => 1
        );

        // then make the call to the delivery/{id}/quote endpoint using the POST method
        $deliveryPostQuoteResponse = $api->deliveryPostQuote($id, $data);
        
        // make sure the post succeeded
        if($deliveryPostQuoteResponse !== null && $deliveryPostQuoteResponse["success"]) {
            
            // Finally we can select the preferred service for the delivery quote.
            
            // variable for keeping the response
            $deliveryPutQuoteResponse = null;
            
            // values to pass as request body
            $data = array(
                // we will select the first service in the list of services/rates received
                "service" => $deliveryPostQuoteResponse["data"]["rates"][0]["service"]
            );

            // then make the call to the delivery/{id}/quote endpoint using the PUT method
            $deliveryPutQuoteResponse = $api->deliveryPutQuote($id, $data);
            var_dump($deliveryPutQuoteResponse);
            
        }
        
    }
}
?>
</pre>

<h2>Dispatch Delivery</h2>
<p>Sample for dispatching a delivery using the <code>delivery/{id}/dispatch</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// First, create a delivery.

// variable for keeping the response
$deliveryPostResponse = null;

// values to pass as request body
$data = array(
    "delivery_courier_checked" => 0,
    "delivery_contact_name" => "Test",
    "delivery_contact_number_1" => "1231231234",
    "delivery_street" => "2 Street Ave",
    "delivery_suburb" => "Suburb",
    "delivery_city" => "City",
    "delivery_province" => "Gauteng",
    "delivery_country" => "South Africa",
    "delivery_postal_code" => "1501"
);

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id} endpoint using the GET method
    $deliveryPostResponse = $api->deliveryPost($data);
    
    // make sure the post succeeded
    if($deliveryPostResponse !== null && $deliveryPostResponse["success"]) {
        
        // Next, call the quote endpoint for this delivery.
        
        // variable for keeping the response
        $deliveryPostQuoteResponse = null;
        
        // delivery id
        $id = $deliveryPostResponse["data"]["id"];

        // values to pass as request body
        $data = array(
            "value" => "199",
            "products" => array(
                array(
                    "quantity" => 1, // integer		  Y	number of pieces
                    "description" => "String with max length of 30", // string[30]	  N	freight description
                    "height" => 100, // integer		  Y	dimension 1 in millimeters
                    "width" => 250, // integer		  Y	dimension 2 in millimeters
                    "length" => 100, // integer		  Y	dimension 3 in millimeters
                    "weight" => 1.0 // float		  Y	mass in kilograms
                )
            ),
            "insurance" => 1
        );

        // then make the call to the delivery/{id}/quote endpoint using the POST method
        $deliveryPostQuoteResponse = $api->deliveryPostQuote($id, $data);
        
        // make sure the post succeeded
        if($deliveryPostQuoteResponse !== null && $deliveryPostQuoteResponse["success"]) {
            
            // Thirdly, select the preferred service for the delivery quote.
            
            // variable for keeping the response
            $deliveryPutQuoteResponse = null;
            
            // values to pass as request body
            $data = array(
                // we will select the first service in the list of services/rates received
                "service" => $deliveryPostQuoteResponse["data"]["rates"][0]["service"]
            );

            // then make the call to the delivery/{id}/quote endpoint using the PUT method
            $deliveryPutQuoteResponse = $api->deliveryPutQuote($id, $data);
            
            // make sure the put succeeded
            if($deliveryPutQuoteResponse !== null && $deliveryPutQuoteResponse["success"]) {
                
                // Finally, we can dispatch the courier.
                
                // make the call to the delivery/{id}/dispatch endpoint using the PUT method
                $deliveryPutDispatchResponse = $api->deliveryPutDispatch($id, null);
                var_dump($deliveryPutDispatchResponse);
                
            }
            
        }
        
    }
}
?>
</pre>

<h2>Get Delivery Waybill</h2>
<p>Sample for downloading the delivery's waybill using the <code>delivery/{id}/waybill</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$deliveryGetWaybillResponse = null;

// delivery id
$id = ($deliveryPostResponse !== null && $deliveryPostResponse["success"] ? $deliveryPostResponse["data"]["id"] : "{delivery id}");

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id}/waybill endpoint using the GET method
    $deliveryGetWaybillResponse = $api->deliveryGetWaybill($id);
    var_dump($deliveryGetWaybillResponse);
}
?>
</pre>

<h2>Get Delivery Label</h2>
<p>Sample for downloading the delivery's label using the <code>delivery/{id}/label</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$deliveryGetLabelResponse = null;

// delivery id
$id = ($deliveryPostResponse !== null && $deliveryPostResponse["success"] ? $deliveryPostResponse["data"]["id"] : "{delivery id}");

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id}/label endpoint using the GET method
    $deliveryGetLabelResponse = $api->deliveryGetLabel($id);
    var_dump($deliveryGetLabelResponse);
}
?>
</pre>

<h2>Get Delivery Tracking</h2>
<p>Sample for getting the delivery's tracking info using the <code>delivery/{id}/track</code> endpoint.</p>
<pre style="max-height: 500px; overflow: auto; background-color: lightgray;">
<?php
include_once "includes/WebRequest.php";
include_once "includes/AppturePayAPI.php";

$clientId = "{your client id}";
$clientSecret = "{your client secret}";

// get an instance of the AppturePayAPI class
$api = new ApptureLab\AppturePayAPI($clientId, $clientSecret);

// variable for keeping the response
$deliveryGetTrackResponse = null;

// delivery id
$id = ($deliveryPostResponse !== null && $deliveryPostResponse["success"] ? $deliveryPostResponse["data"]["id"] : "{delivery id}");

// make sure we are successfully authenticated
if( $api->getSession() !== null ) {
    // then make the call to the delivery/{id}/track endpoint using the GET method
    $deliveryGetTrackResponse = $api->deliveryGetTrack($id);
    var_dump($deliveryGetTrackResponse);
}
?>
</pre>